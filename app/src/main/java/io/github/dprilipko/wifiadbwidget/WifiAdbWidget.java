package io.github.dprilipko.wifiadbwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.RemoteViews;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.MessageFormat;

import static android.content.Context.WIFI_SERVICE;

/**
 * Implementation of App Widget functionality.
 */
public class WifiAdbWidget extends AppWidgetProvider {

    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverWidget";
    public static String WIDGET_IDS_KEY = "WIDGET_IDS_KEY";
    static MessageFormat howToTemplate = new MessageFormat(
            "1. Connect phone to your PC (via usb)\n" +
            "2. Run command `adb tcpip 5555` in terminal");
    static MessageFormat connectTemplate = new MessageFormat("3. Now run command in terminal\n `adb connect {0}:5555`");

    static boolean showInstructions = false;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);

        String ipAddress = "";// Formatter.formatIpAddress(ip);
        try {
            ipAddress = getIp4Address(context);
        } catch (UnknownHostException e) {
        }
        showInstructions = !showInstructions;

        Log.i("widget", "update");
        String msg = showInstructions ? howToTemplate.format(null) : connectTemplate.format(new String[]{ipAddress});

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.wifi_adb_widget);
        views.setTextViewText(R.id.appwidget_text, msg);

        // create and register event
        Intent active = new Intent();
        active.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = new int[]{appWidgetId};
        active.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0,
                active, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.container, actionPendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    static String getIp4Address(Context context) throws UnknownHostException, NullPointerException {
        WifiManager wifiMgr = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        byte[] bytes = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ip).array();
        InetAddress address = InetAddress.getByAddress(bytes);
        return address.getHostAddress();
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        Log.i("widget", "onReceive");

//        if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(action)) {
//            showInstructions = !showInstructions;
//            int[] ids = intent.getExtras().getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);
//            this.onUpdate(context, AppWidgetManager.getInstance(context), ids);
//        }
        super.onReceive(context, intent);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

